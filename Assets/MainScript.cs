﻿//using SimpleFirebaseUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScript : MonoBehaviour {

    public GameObject camera;

    public GameObject oxygenLevelText;
    public GameObject heartRateText;
    public GameObject temperatureText;

    private int oxygenLevel = 78;
    private int heartRate = 110;
    private int temperature = 55;

    //private Firebase firebase;

	// Use this for initialization
	void Start () {
        /*firebase = Firebase.CreateNew("square-34a5a.firebaseio.com");

        firebase.OnSetSuccess = new System.Action<Firebase, DataSnapshot>((Firebase firebase, DataSnapshot data) =>
        {
            Debug.Log("FIREBASE SUCCESS. Updated Key = " + data.FirstKey);
        });

        firebase.OnSetFailed = new System.Action<Firebase, FirebaseError>((Firebase firebase, FirebaseError error) =>
        {
            Debug.Log("FIREBASE FAILED. Message = " + error.Message);
        });

        firebase.SetValue("{ \"key\": \"rotation\", \"value\": \"1\"}", true);*/
    }
	
	// Update is called once per frame
	void Update () {
        /*if (Time.fixedTime % .5 < .5)
        {
            firebase.SetValue("{ \"key\": \"rotation\", \"value\": \"" + camera.transform.rotation.eulerAngles.x + "\"}", true);
            firebase.SetValue("{ \"key\": \"positionx\", \"value\": \"" + camera.transform.rotation.eulerAngles.z + "\"}", true);
            firebase.SetValue("{ \"key\": \"positiony\", \"value\": \"" + camera.transform.rotation.eulerAngles.y + "\"}", true);

            Debug.Log("FIREBASE TEST. Rotation = " + camera.transform.rotation.eulerAngles.x);
            Debug.Log("FIREBASE TEST. Position X = " + camera.transform.rotation.eulerAngles.z);
            Debug.Log("FIREBASE TEST. Position Y = " + camera.transform.rotation.eulerAngles.y);
        }*/

        if (Time.fixedTime % 5 < .001)
        {
            oxygenLevel--;
            oxygenLevelText.GetComponent<Text>().text = "Oxygen Level: " + oxygenLevel + "%";

            heartRateText.GetComponent<Text>().text = "Heart Rate: " + Random.Range(80, 120) + " bpm";

            if (Random.Range(0, 100) >= 50)
            {
                temperature = temperature - Random.Range(5, 10);
            }
            else
            {
                temperature = temperature + Random.Range(5, 10);
            }

            temperatureText.GetComponent<Text>().text = "Temperature: " + temperature + "°C";
        }
    }
}
