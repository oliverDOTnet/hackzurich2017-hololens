﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.VR.WSA.Input;
using UnityEngine.Windows.Speech;

public class Camera : MonoBehaviour {

    private GestureRecognizer _gestureRecognizer;
    private KeywordRecognizer _keywordRecognizer;
    private Dictionary<string, Action> _keywords;

    public Material rescuedMaterial;
    public GameObject path;
    public GameObject path1;
    public GameObject path2;
    public GameObject matt;

    // Use this for initialization
    void Start()
    {
        _gestureRecognizer = new GestureRecognizer();
        _gestureRecognizer.TappedEvent += _gestureRecognizer_TappedEvent;
        //_gestureRecognizer.HoldStartedEvent += ;
        _gestureRecognizer.StartCapturingGestures();

        _keywords = new Dictionary<string, Action>
        {
            {
                "Test", () => {  }
            }
        };
        _keywordRecognizer = new KeywordRecognizer(_keywords.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += OnPhraseRecognized;
        _keywordRecognizer.Start();
    }

    private int n = 0;

    private void _gestureRecognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        n++;

        if (n == 1)
        {
            path.SetActive(true);
        }
        else if (n == 2)
        {
            path1.SetActive(false);
            path2.SetActive(false);

            matt.GetComponent<Renderer>().material = rescuedMaterial;
        }
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (_keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    void Update ()
	{

    }
}
